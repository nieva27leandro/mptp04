import os


def pulsarTecla():
    input('Pulse un tecla...')    

def menu():
    os.system('cls')
    print('1) Cargar productos')
    print('2) Mostrar lista de productos')
    print('3) Mostrar productos de stock en intervalo A-B')
    print('4) Sumar un X a productos con stock menor a un valor Y')
    print('5) Eliminar productos de stock igual a cero')
    print('6) Salir')
    opcion = int(input('Elija una opción: '))
    while not((opcion >= 1) and (opcion <= 7)):
        opcion = int(input('Elija una opción: '))
    os.system('cls')
    return opcion

def leerProductos():
    print('Cargar Lista de productos')
    productos = {}
    codigo = -1
    while (codigo != 0):
        codigo = int(input('Ingrese el codigo del producto (cero para finalizar): '))
        if codigo != 0: 
            if codigo not in productos:    
                descripcion = input('Descripcion: ')
                precio = leerPrecio()
                stock = leerStock()
                productos[codigo] = [descripcion,precio,stock]
                print('agregado correctamente')
            else:
                print('el producto ya existe')
    return productos

def leerPrecio():
    precio = float(input('Precio: '))
    if precio<=0:
            print('No es válido, ingrese un valor positivo')  
            precio = float(input('Precio: '))     
    return precio

def leerStock():
    stock = int(input('Stock: '))
    if stock<0:
            print('No es válido, ingrese un valor positivo')
            stock = int(input('Stock: '))       
    return stock

def mostrar(productos):
    print('Listado de productos')
    for clave, valor in productos.items():
        print(clave,valor)

def mostrarIntervalo(diccionario):
    A= int(input('Ingrese el valor de stock mínimo (A) del intervalo: '))
    B= int(input('Ingrese el valor de stock máximo (B) del intervalo: '))
    cant=0
    if A<B:
        print('Los productos con stock dentro de su intervalo son: ')
        for clave,valor in diccionario.items():
            if valor[2] in range(A,B+1):
                print(clave,valor)
                cant+=1
        print('cantidad de productos',cant)            
    else:
        print('Verifique el máximo y el mínimo!')

def sumarStock(productos):
    Y = int(input('Ingrese el valor de Y(maximo stock a sumar): '))
    X = int(input('Ingrese valor a agregar(X): '))
    for clave,valor in productos.items():
        if valor[2]<=Y:
            valor[2]+=X
    print('Se modifico correctamente el listado')        
    
def eliminarStockCero(diccionario): 
    logic=False
    productos={}
    confirmar='.'
    print('Se eliminaran los siguientes productos: ')
    for clave,valor in diccionario.items():
        if valor[2]==0:
            logic=True
            print(clave,valor)
    if logic:
        confirmar=input(print('Esta seguro de eliminar los items(s/n)?: '))
        if confirmar=='s':
            for clave,valor in diccionario.items():
                if valor[2]==0:
                    print('Elemento eliminado')
                else:
                    productos[clave]=valor                
        elif confirmar=='n': 
            print('Cancelado')
            productos=diccionario
        else:
            confirmar=input(print('Esta seguro de eliminar los items(s/n)?: '))            
    else:
        print('No hay productos sin stock') 
    
    return productos
    
def salir():
    print('Adios')

opcion = 0

while (opcion != 6):
    opcion = menu()
    if opcion == 1:
        productos = leerProductos()
    elif opcion == 2:
        mostrar(productos)
        pulsarTecla()
    elif opcion == 3:
        mostrarIntervalo(productos)
        pulsarTecla()
    elif opcion == 4:
        sumarStock(productos)
        pulsarTecla()
    elif opcion == 5:
        productos=eliminarStockCero(productos)
        pulsarTecla()
    elif opcion == 6:     
        salir()
    
